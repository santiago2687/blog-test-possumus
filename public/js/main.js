$(document).ready(function() {
    $("#posts-table").dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/getAllPosts"
        },
        "rowCallback": function( row, data) {      
            var publish_post;
            if(data.published == 0) {
                publish_post = '<div style="float:left;margin-left:6%;">'+
                '<a href="/publishPost/'+data.id+'" id="publish_post" class="btn btn-block btn-success">Publish</a></div>';
            } else {
                publish_post = '<div style="float:left;margin-left:6%;">'+
                '<a href="/unPublishPost/'+data.id+'" id="publish_post" class="btn btn-block btn-danger">Unpublish</a></div>';
            }

            console.log(publish_post);

            $(row).append('<td class="action_td" style="width:25%;"><div style="float:left;margin-left:6%;">'+
                              '<a href="/post/'+data.id+'" class="btn btn-block btn-primary">Preview</a></div>'+
                              '<div style="float:left;margin-left:6%;"><a id="editar_paciente" class="btn btn-block btn-primary" href="/editPost/'+data.id+'">Edit</a></div>'+
                              ''+publish_post+''+
                              '</td>');
        },

        columns: [
            { data: "id", name: "id" },

            { data: "title", name: "title" },

            { data: "created_at", name: "created_at" },

            { data: "updated_at", name: "updated_at" }
        ]
    });

    $("#home_image").on("change", function() {
        readURL($(this));
    });

    $("#post_image").on("change", function() {
        
        readURL($(this));
    });

    $("#edit_post_image").on("change", function() {
        readURL($(this));
    });

    var txt= $('.post_text_content').text();
    if(txt.length > 155){
        $('.post_text_content').text(txt.substring(0,150) + '.....');
    }
    
});

function readURL(input) {
    console.log(input.files);
    if (input.files && input.files[0]) {
        console.log("hola");
        var reader = new FileReader();
        if (input.name == "post_image") {
            reader.onload = function(e) {
                $("#preview_post_image").attr("src", e.target.result);
            };
        } else if (input.name == "home_image") {
            reader.onload = function(e) {
                $("#preview_home_image").attr("src", e.target.result);
            };
        } else if(input.name == "edit_post_image") {
            alert("hola");
            reader.onload = function(e) {
                $("#preview_post_edit_image").attr("src", e.target.result);
            };
        }
        reader.readAsDataURL(input.files[0]);
    }
}
