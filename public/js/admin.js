$('#posts-table').DataTable({

    processing: true,

    serverSide: true,

    ajax: "{!! route('getAllPosts') !!}",
    
    columns: [

        { data: 'id', name: 'id' },

        { data: 'title', name: 'title' },

        { data: 'created_at', name: 'created_at' },

        { data: 'updated_at', name: 'updated_at' }
    ]

});