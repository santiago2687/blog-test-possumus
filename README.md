# Blog Test Possumus

Prueba Técnica para el puesto de desarrollador Web PHP. 
Se realizó con:

Lenguaje: PHP 7.3
Laravel: 5.6
Boostrap: 3.3.7
Jquery: 3
Mysql / MariaDB
Composer
NPM
Artisan
GIT

Y demas librerias para la validacion de formularios, clases para web responsive y demas...

NOTA: editar el PHP.ini de la version de PHP correspondiente y reemplazar el del maximo de tamaño de archivos de subida de 2M a 10M. Esto lo tuve que realizar ya que 2M era muy poco para la subida hoy en dia.

; Maximum allowed size for uploaded files.
; http://php.net/upload-max-filesize
upload_max_filesize = 10M (Valor original 2M)

Solo deben clonar el proyecto y correr "composer install", "composer update", luego crear la DB y luego correr los migrations y los seeders correspondientes. 
Con esto deberia bastar para levantar la web. Ya luego si es entorno de windows correr comando dentro del proyecto.. "php artisan serve" para levantar el ambiente local del proyecto.