@extends('layouts.app')

@section('content')
<div class="container">

    <section>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="justify-content-center">
                    <h1 class="title-align-center">Edit Post</h1>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row justify-content-center">

            <div class="col-md-8 col-md-8 col-md-offset-2">
                <form data-toggle="validator" id="formCreatePost" action="{{url('postEditPost')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    <input type="hidden" id="id_post" name="id_post" value="{{ $post->id }}">
                    <div class="form-group had-feedback">
                        <label for="">Post title</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ @$post->title }}" placeholder="Enter new Title Blog" required
                        data-error="The title is required">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group had-feedback">
                        <label for="">Text Body Post</label>
                        <textarea class="form-control h-300 resize-none-text-area" name="text" id="text-post" row="10" col="10" required
                        data-error="The text is required">{{ @$post->text }}</textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group had-feedback">
                        <input type="file" class="form-control-file" id="edit_post_image" name="edit_post_image" value="" onchange="readURL(this);">
                        <br>
                        @if (file_exists(public_path().'/post_images/'.$post->path_image))
                        <img class="home_img" id="preview_post_edit_image" alt="your image" width="180" height="180" src="{{ asset('post_images').'/'.$post->path_image }}" />
                        @else
                        <img class="home_img" id="preview_post_edit_image" alt="your image" width="180" height="180" src="{{ asset('web_images').'/placeholder.png' }}" />
                        @endif
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3 had-feedback">
                            <label for="">Draft</label>
                            <input type="radio" value="0" {{ ($post->published == "0")? "checked" : "" }} name="published"
                            data-error="The title is required">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-3 had-feedback">
                            <label for="">Published</label>
                            <input type="radio" value="1" {{ ($post->published == "1")? "checked" : "" }} name="published"
                            data-error="The title is required">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary w-100">Save changes</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

</div>
@endsection