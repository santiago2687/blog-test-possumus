@extends('layouts.app')

@section('content')
<div class="container">

    <section>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="justify-content-center">
                    <h1 class="title-align-center">Control Panel</h1>
                </div>
            </div>
        </div>
    </section>

    <hr>
    <br>

    <section>
        <div class="row justify-content-left">
            <div class="col-md-12">
                <h1>General Settings</h1>
            </div>

            <div class="col-md-12">
                <form data-toggle="validator" role="form" id="formGeneralSettings" action="{{url('updateGeneralSettings')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="form-group had-feedback">
                        <label for="">Web title</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ @$generalSettings->title }}" required
                        data-error="The Web title is required">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="">Home Image File</label>
                        <input type="file" class="form-control-file" id="home_image" name="home_image" placeholder="Enter new Title Blog" value="{{public_path().'/web_images/'.$generalSettings->home_image}}" onchange="readURL(this);">
                        <br>
                        @if (file_exists(public_path().'/web_images/'.$generalSettings->home_image))
                        <img class="home_img" id="preview_home_image" alt="your image" width="180" height="180" src="{{ asset('web_images').'/'.$generalSettings->home_image }}" />
                        @else
                        <img class="home_img" id="preview_home_image" class="preview_image" alt="your image" width="180" height="180" src="{{ asset('web_images').'/placeholder.png' }}" />
                        @endif

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary w-100">Save changes</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </section>

    <br>
    <hr>
    <br>
    <br>
    <br>

    <section>
        <div class="row justify-content-left">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{url('createPost')}}" class="btn btn-primary w-100">Add new Post</a>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <h1>List Posts</h1>

                <table id="posts-table" class="table table-striped table-bordered" style="width:100%">

                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                </table>

            </div>
        </div>
    </section>
</div>
<br>
<br>
<br>
@endsection



