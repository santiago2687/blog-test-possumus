@extends('layouts.app')

@section('content')
<div class="container">

    <section>
        <div class="row justify-content-center">
            <div class="col-md-12 col-md-offset-2">
                <div class="justify-content-center">
                    <h1 class="w-100">Add New Post</h1>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row justify-content-center">

            <div class="col-md-8 col-md-offset-2">
                <form data-toggle="validator" id="formCreatePost" action="{{url('createPost')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="form-group had-feedback">
                        <label for="">Post title</label>
                        <input type="text" class="form-control" id="title" name="title" value="" required
                        data-error="The title is required">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group had-feedback">
                        <label for="">Text Body Post</label>
                        <textarea class="form-control h-300 resize-none-text-area" name="text" id="text-post" row="10" col="10" required
                        data-error="The text is required"></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group had-feedback">
                        <input type="file" class="form-control-file" id="post_image" name="post_image" value="" onchange="readURL(this);" required>
                        <br>
                        <img class="home_img" id="preview_post_image" class="preview_image" alt="your image" width="180" height="180" src="{{ asset('web_images').'/placeholder.png' }}" />
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3 had-feedback">
                            <label for="">Draft</label>
                            <input type="radio" value="0" name="published" required
                            data-error="The status is required">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-3 had-feedback">
                            <label for="">Published</label>
                            <input type="radio" value="1" name="published" required
                            data-error="The status is required">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary w-100">Add Post</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

</div>
@endsection
