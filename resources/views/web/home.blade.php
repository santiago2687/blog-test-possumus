@extends('layouts.app')

@section('content')
<div class="container">
    <section>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="justify-content-center">
                    <h1 class="title-align-center">{{ $generalSettings->title }}</h1>
                </div>
                <br>
                <div>
                    <img src="{{ asset('web_images').'/'.$generalSettings->home_image }}" class="w-100" alt="profile Pic">
                </div>

            </div>
        </div>
    </section>
    <br>
    <br>
    <section>
        <div class="row justify-content-left">
            <div class="col-md-12">
                <h1>List Posts</h1>
            </div>
        </div>
        <hr>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-12">

                @foreach($posts as $post)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $post->title }}
                        <div class="pull-right">Published Date: {{$post->created_at}}</div>
                    </div>
                    <div class="panel-body">
                        @if( $post->path_image )
                        <img src="{{ asset('post_images').'/'.$post->path_image }}" class="img-responsive w-100" alt="">
                        <br>
                        @endif
                        <div class="post_text_content">
                            {{ $post->text }}
                        </div>
                        <br>
                        <a href="{{ url('post/'.$post->id) }}" class="pull-right">Leer Más...</a>
                    </div>
                </div>
                @endforeach

                {{ $posts->render() }}

            </div>
        </div>
    </section>


</div>
@endsection