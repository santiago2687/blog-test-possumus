@extends('layouts.app')

@section('content')
<div class="container">
    <section>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1 class="title-align-center">Post Detail</h1>
            </div>
        </div>
        <hr>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $post->title }}
                        <div class="pull-right">Published Date: {{$post->created_at}}</div>
                    </div>
                    <div class="panel-body">
                        @if( $post->path_image )
                        <img src="{{ asset('post_images').'/'.$post->path_image }}" class="img-responsive w-100" alt="">
                        <br>
                        @endif
                        <div class="post_text_content_full">
                            {{ $post->text }}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
@endsection