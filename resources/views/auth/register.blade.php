@extends('layouts.app')

@section('content')
<div class="container">
    <div id="signupbox" style="display:block; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">{{ __('Register') }}</div>
            </div>
            <div class="panel-body">
                <form data-toggle="validator" id="signupform" novalidate class="form-horizontal" role="form" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                    @csrf
                    <div id="signupalert" style="display:none" class="alert alert-danger">
                        <p>Error:</p>
                        <span></span>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="firstname" class="col-md-3 control-label">{{ __('Name') }}</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name" placeholder="Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus
                            data-error="The name is invalid and required">
                            <div class="help-block with-errors"></div>
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="email" class="col-md-3 control-label">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" placeholder="Email Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required
                            data-error="The email address is invalid and required">
                            <div class="help-block with-errors"></div>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="password" class="col-md-3 control-label">{{ __('Password') }}</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required
                            data-error="The password is invalid and required">
                            <div class="help-block with-errors"></div>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="password" class="col-md-3 control-label">{{ __('Confirm Password') }}</label>
                        <div class="col-md-9">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required
                            data-error="The field is invalid and required">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="role" class="col-md-3 control-label">Role</label>
                        <div class="col-md-9">
                            <select id="role_id" name="role_id" class="form-control{{ $errors->has('role_id') ? ' is-invalid' : '' }}" value="{{ old('role_id') }}" required
                            data-error="The role is required">
                                <option value="">Select a role...</option>
                                @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('role_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('role_id') }}</strong>
                            </span>
                            @endif
                            <div class="help-block with-errors"></div>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection