<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generalSettings')->insert([
            'title' => 'Welcome to MyBlog',
            'home_image' => 'img_home.jpg'
        ]);
    }
}
