<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\GeneralSetting;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $posts = Post::orderBy('created_at', 'desc')->where('published', 1)->paginate(10);
        $generalSettings = GeneralSetting::getAll();
        return view('admin.index', compact('posts', 'generalSettings'));
    }

    public function getAllPosts()
    {
        return Datatables::of(Post::query())->make(true);
    }

    //Create a new post published as default to be visible in the list of post in the web
    public function createPost(Request $request)
    {
        if ($request->method() == "GET") {
            return view(('admin.createPost'));
        } else {
            $newPost = new Post();
            $infoUser = Auth::user();

            $newPost->title = $request->title;
            $newPost->text = $request->text;
            $newPost->user_id = $infoUser->id;
            $newPost->published = $request->published;

            //Behavior of Post Image
            if ($request->file('post_image')) {
                $allPosts = Post::getAllPosts();
                $nro_img_post = count($allPosts) + 1;
                $image = $request->file('post_image');
                $originalName = 'post_image_' . $nro_img_post . 'jpg';
                $destinationPath = public_path('/post_images');
                $image->move($destinationPath, $originalName);

                $newPost->path_image = $originalName;
            }

            $newPost->save();
            alertify()->success('Se creó correctamente el Post!!')->position('bottom right');
            return redirect('adminPanel');
        }
    }

    //Edit selected Post
    public function editPost($id)
    {
        $post = Post::find($id);
        return view('admin.editPost', compact('post'));
    }

    public function postEditPost(Request $request)
    {
        $editPost = Post::find($request->id_post);
        $editPost->title = $request->title;
        $editPost->text = $request->text;
        $editPost->published = $request->published;

        $current_image_path = public_path('post_images') . '\\' . $editPost->path_image;

        if ($request->file('edit_post_image')) {
            if (file_exists($current_image_path) || $current_image_path != '') {
                File::delete($current_image_path);
                $image = $request->file('edit_post_image');
                $originalName = 'post_image_' . $editPost->id . '.jpg';
                $destinationPath = public_path('/post_images');
                $image->move($destinationPath, $originalName);
            } else {
                $image = $request->file('home_image');
                $originalName = 'post_image_' . $editPost->id . '.jpg';
                $destinationPath = public_path('/post_images');
                $image->move($destinationPath, $originalName);
            }
            $editPost->path_image = $originalName;
        }
        alertify()->success('Se editó correctamente el Post!!')->position('bottom right');
        $editPost->save();

        return redirect('adminPanel');
    }

    //Function that upublish the post selected and is visible in the blog
    public function publishPost($id)
    {
        //Validate if the post id exists
        $post = Post::find($id);
        if ($post) {
            $post = Post::find($id);
            $post->published = 1;
            $post->save();
            alertify()->success('Se publicó correctamente el Post!!')->position('bottom right');
            return redirect()->back();
        } else {
            alertify()->error('Hubo unu inconveniente y no se ha podido publicar el Post!!')->position('bottom right');
            return redirect()->back();
        }
    }

    //Function that unpublish the post selected and is hidden in the blog
    public function unPublishPost($id)
    {
        //Validate if the post id exists
        $post = Post::find($id);
        if ($post) {
            $post->published = 0;
            $post->save();
            alertify()->success('Se despublicó correctamente el Post!!')->position('bottom right');
            return redirect()->back();
        } else {
            alertify()->success('Hubo unu inconveniente y no se ha podido despublicar el Post!!')->position('bottom right');
            return redirect()->back();
        }
    }

    //Function that update the title and home image from the blog
    public function updateGeneralSettings(Request $request)
    {
        //get title and home image name from DB
        $generalSettings = GeneralSetting::getAll();
        $fileName = $generalSettings->home_image;
        $current_image_path = public_path('web_images') . '\\' . $fileName;

        //validation if exist file from request and the home image is named equal always
        if ($request->file('home_image')) {
            if (file_exists($current_image_path) || file_exists(public_path('/web_images') . '/img_home.png') || $current_image_path != '') {
                File::delete($current_image_path);
                $image = $request->file('home_image');
                $originalName = ENV('CONSTANT_IMG_HOME_NAME');
                $destinationPath = public_path('/web_images');
                $image->move($destinationPath, $originalName);
            } else {
                $image = $request->file('home_image');
                $originalName = ENV('CONSTANT_IMG_HOME_NAME');
                $destinationPath = public_path('/web_images');
                $image->move($destinationPath, $originalName);
            }
            $generalSettings->home_image = $originalName;
        }

        //if exist title from the request update it
        if (isset($request->title) && $request->title != $generalSettings->title) {
            $generalSettings->title = $request->title;
        }

        $generalSettings->save();
        alertify()->success('Se actualizaron el titulo y la imagen de portada de forma correcta!!')->position('bottom right');

        return redirect()->back();
    }
}
