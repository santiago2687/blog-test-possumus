<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\GeneralSetting;

class HomeController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        //Get all Posts
        $posts = Post::orderBy('created_at', 'desc')->where('published', 1)->paginate(10);
        //Get General Settings (Title and Principal Image)
        $generalSettings = GeneralSetting::getAll();
        return view('web.home', compact('posts',  'generalSettings'));
    }

    public function showPostDetail($idPost)
    {   
        //Get selected Post
        $post = Post::getPost($idPost);
        return view('web.post', compact('post'));
    }
}
