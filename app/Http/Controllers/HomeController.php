<?php

namespace App\Http\Controllers;
use App\Post;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //Get all Posts
        $posts = Post::all();
        return view('web.home')->with(['posts' => $posts]);
    }

    public function showPostDetail($idPost)
    {   dd($idPost);
        //Get selected Post
        //$post = Post::getPost($idPost);
        //return view('web.post')->with(['post' => $post]);
    }
}
