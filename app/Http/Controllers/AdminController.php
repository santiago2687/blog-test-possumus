<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $posts = Post::all();

        return view('admin.index')->with(['posts' => $posts]);
    }

    //Create a new post published as default to be visible in the list of post in the web
    public function createPost(Request $request)
    {

        dd($request);
        $newPost = new Post();
    }

    //Edit Sselected Post
    public function editPost($id)
    {
        dd($id);
        $post = Post::find($id);
    }

    //Function that upublish the post selected and is visible in the blog
    public function publishPost($id)
    {
        try {

            //Validate if the post id exists
            $postExist = Post::existPost($id);

            if ($postExist) {

                $post = Post::find($id);
                $post->published = 1;
                return true;

            } else {

                return false;

            }

        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //Function that unpublish the post selected and is hidden in the blog
    public function unPublishPost($id)
    {
        try {
            
            //Validate if the post id exists
            $postExist = Post::existPost($id);

            if ($postExist) {

                $post = Post::find($id);
                $post->published = 0;

                
            } else {

                return false;

            }

        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function updateTitleBlog(Request $request) 
    {
        dd("hola");
    }

    public function updatePrincipalImageBannerBlog(Request $request) 
    {   
        dd("hola");
    }
}
