<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    protected $fillable = [
        'id', 'title', 'text', 'path_image', 'user_id', 'published', 'created_at', 'updated_at'
    ];

    static function getAllPosts() {
        $posts = Post::all();
        return $posts;
    }

    static function getPost($id) {
        $post = Post::find($id);
        return $post;
    }

}
