<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'generalSettings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'home_image'
    ];

    static function getAll()
    {
        $generalSettings = GeneralSetting::all();
        return $generalSettings[0];
    }
}
