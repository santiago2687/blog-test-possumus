<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {

    Auth::routes();

    Route::get('/', 'Web\HomeController@index')->name('home');
    Route::get('/post/{id}', 'Web\HomeController@showPostDetail')->name('post');
    Route::get('/home', 'Web\HomeController@index')->name('home');

    // Users Administrator Routes...
    Route::get('/adminPanel', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@index'])->name('adminPanel');
    Route::get('/createPost', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@createPost'])->name('createPost');
    Route::get('/editPost/{id}', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@editPost'])->name('editPost');
    Route::get('/publishPost/{id}', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@publishPost'])->name('publishPost');
    Route::get('/unPublishPost/{id}', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@unPublishPost'])->name('unPublishPost');
    Route::get('/getAllPosts', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@getAllPosts'])->name('getAllPosts');
    
    Route::Post('/updateGeneralSettings', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@updateGeneralSettings'])
    ->name('updateGeneralSettings');
    Route::post('/createPost', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@createPost'])->name('createPost');
    Route::post('/postEditPost', ['middleware'=> ['auth'], 'uses' => 'Admin\AdminController@postEditPost'])->name('postEditPost');


});
